/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sibs.manager;

/**
 * @author miche
 */

import org.snmp4j.agent.ManagedObject;
import org.snmp4j.security.*;
import org.snmp4j.security.nonstandard.PrivAES192With3DESKeyExtension;
import org.snmp4j.security.nonstandard.PrivAES256With3DESKeyExtension;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;

import java.io.IOException;

public class SnmpAgentV3 {

    public static class Credentials {
        private String userName;
        private SecurityLevel securityLevel;
        private String authProtocol;
        private String authPassword;
        private String privProtocol;
        private String privPassword;

        public Credentials(String userName, SecurityLevel securityLevel, String authProtocol, String authPassword,
                           String privProtocol, String privPassword) {
            this.userName = userName;
            this.securityLevel = securityLevel;
            this.authProtocol = authProtocol;
            this.authPassword = authPassword;
            this.privProtocol = privProtocol;
            this.privPassword = privPassword;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public SecurityLevel getSecurityLevel() {
            return securityLevel;
        }

        public void setSecurityLevel(SecurityLevel securityLevel) {
            this.securityLevel = securityLevel;
        }

        public String getAuthProtocol() {
            return authProtocol;
        }

        public void setAuthProtocol(String authProtocol) {
            if (!this.authProtocol.equals(authProtocol)) {
                this.authProtocol = authProtocol;
            }
        }

        public String getAuthPassword() {
            return authPassword;
        }

        public void setAuthPassword(String authPassword) {
            this.authPassword = authPassword;
        }

        public String getPrivProtocol() {
            return privProtocol;
        }

        public void setPrivProtocol(String privProtocol) {
            this.privProtocol = privProtocol;
        }

        public String getPrivPassword() {
            return privPassword;
        }

        public void setPrivPassword(String privPassword) {
            this.privPassword = privPassword;
        }

    }

    private final Credentials credentials;
    private final OctetString contextName;

    public SnmpAgentV3(Credentials credentials, ManagedObject... objects) throws IOException {
        this.credentials = credentials;
        this.contextName = new OctetString();
    }

    public static OID getAuthProtocol(String authProtocol) {
        switch (authProtocol) {
            case "MD5":
                return AuthMD5.ID;
            case "SHA":
                return AuthSHA.ID;
            case "SHA224":
                return AuthHMAC128SHA224.ID;
            case "SHA256":
                return AuthHMAC192SHA256.ID;
            case "SHA384":
                return AuthHMAC256SHA384.ID;
            case "SHA512":
                return AuthHMAC384SHA512.ID;
            default:
                return null;
        }
    }

    public static OID getPrivacyProtocol(String privProtocol) {
        switch (privProtocol) {
            case "DES":
                return PrivDES.ID;
            case "AES":
                return PrivAES128.ID;
            case "AES192":
                return PrivAES192.ID;
            case "AES256":
                return PrivAES256.ID;
            case "AES192C":
                return PrivAES192With3DESKeyExtension.ID;
            case "AES256C":
                return PrivAES256With3DESKeyExtension.ID;
            default:
                return null;
        }
    }

    public OctetString getContextName() {
        return this.contextName;
    }

    public void setUserName(String userName) throws IOException {
        if (!credentials.getUserName().equals(userName)) {
            credentials.setUserName(userName);
        }
    }


}
